# Infrastructure

Repository contains Infrastructure configuration for `dev`, `stage`, `prod`
environments. Each one located is in respected directory.

## Prerequisites
Copy `.env.example` into the respective environment directory as `.env`.
```
cp .env.example <env>/.env
```
Update the variables in `.env`.
When changing directories environment variables will be read from `.env` file.

## Starting deploy container
```
docker-compose run --rm app
```
Above command will start interactive shell in
[auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image).
Container contains all the necessary tools such as `kubectl`, `helm`,
`terraform`.

## Terraform
In the environment directory run:
```
ENV=${PWD##*/} envsubst < backend.tf.template > backend.tf
terraform init
```
