source <(kubectl completion bash)
source <(helm completion bash)
complete -C /usr/bin/aws_completer aws
complete -C /usr/bin/terraform terraform
